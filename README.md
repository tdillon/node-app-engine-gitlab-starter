# Node App Engine GitLab Starter

The canonical example of how to create a Node app for App Engine using GitLab.

## Prerequisites

You should have a Google account, and access to Google Cloud Platform (https://console.cloud.google.com/).
You have an empty GitLab project cloned locally.

## Project Setup

```shell
npm init -y
npm i express
```

Add `"start": "node src/app.js"` to the scripts section of your `package.json` file.

Copy the following files of this repo into your project.

- src/app.js
- .gcloudignore
- .gitignore
- .gitlab-ci.yml
- app.yaml

## App Engine Setup

Go to https://console.cloud.google.com.

- Click the project dropdown and choose **New Project**.
- Give it a name (e.g., `My Great App`) and ID.
  *Note* I recommend using kabob case of the name for the id (e.g., my-great-app).
  Click **Create**.
- Switch to the new project (using dropdown or link) in GCP.
- Follow instructions here to setup keys/tokens/accounts: https://circleci.com/docs/2.0/google-auth/
  - Go to the service accounts page in GCP (*IAM & admin -> Service accounts*)
  - Click **Create Service Account**
  - *Name*: **GitLab CI**
  - *ID*: **gitlab-ci**
  - *Description*: **Account used by GitLab for deploying to App Engine.**
  - Click **Create**.
  - Add the following *roles*.
    - App Engine Deployer
    - App Engine Service Admin
    - Cloud Build Service Account
    - Storage Object Creator
    - Storage Object Viewer 
  - Click **Continue**
  - Click **Create Key**
    - **JSON**
    - Click **Create**
    - Save file to your machine.
    - Click **Done**
    - Setup GitLab variables (Settings -> CI/CD -> Variables)
      - Create `GCLOUD_SERVICE_KEY` variable in your GitLab project.
      - Set the value of `GCLOUD_SERVICE_KEY` variable to the value of the JSON file's contents.
      - Create `GOOGLE_PROJECT_ID` variable in GitLab. Set the value accordingly (e.g., **my-great-app**).
      - Click **Save variables**.
  - Create App Engine App in your GCP Project
    - From *Cloud Shell* within GCP run `gcloud app create` (pick your region accordingly, e.g,. us-central).
  - From GCP, go to **APIs & Services -> Dashboard**, you'll need to do this twice for the 2 apis below.
    - Click **Enable APIs and Services**.
    - Search for and select **Cloud Build API**
      - Click **Enable**
    - Search for and select **App Engine Admin API**
      - Click **Enable**

## Kick off a build

Everything should be setup now.
Commit and push your changes.
After building, your app should be live.
